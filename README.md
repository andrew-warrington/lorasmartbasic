# Getting your RM186 LoRa/BLE board to work with The Things Network


## Setting up the Laird RM186 LoRa board

The following documents were very helpful in getting things working:

 - [Loading and Running Applications with UwTerminalX - RM1xx Series.pdf](https://assets.lairdtech.com/home/brandworld/files/Loading%20and%20Running%20Applications%20with%20UwTerminalX%20-%20RM1xx%20Series.pdf)
 - [Datasheet (HIG) - RM1xx Series.pdf](https://assets.lairdtech.com/home/brandworld/files/Datasheet%20%28HIG%29%20-%20RM1xx%20Series.pdf) 
 - [User Guide - smartBASIC Core Functionality.pdf](https://assets.lairdtech.com/home/brandworld/files/User%20Guide%20-%20smartBASIC%20Core%20Functionality.pdf) 
 - [User Guide - RM1xx smartBASIC Extensions (LoRaFunctions).pdf](https://assets.lairdtech.com/home/brandworld/files/User%20Guide%20-%20RM1xx%20smartBASIC%20Extensions%20%28LoRa%20Functions%29.pdf)  
 - [Interfacing with LoRaWAN - RM186 v1.3.pdf](https://connectivity-staging.s3.us-east-2.amazonaws.com/s3fs-public/2018-10/Interfacing%20with%20LoRaWAN%20-%20RM186%20v1.3.pdf)
 - [Github RM1xx-Applications](https://github.com/LairdCP/RM1xx-Applications)
 - [smartBASIC-VS-Code-Extension](https://github.com/LairdCP/RM1xx-Applications) (Only if you like to use Visual Studio Code)
 - [Using BLE and LoRa with the RM1xx](https://assets.lairdtech.com/home/brandworld/files/Using%20BLE%20and%20LoRa%20-%20RM1xx%20Series.pdf)
-  [RM1xx BLE Central smartBASICExtensions](https://assets.lairdtech.com/home/brandworld/files/User%20Guide%20-%20RM1xx%20smartBASIC%20Extensions%20(BLE%20Central%20Functions).pdf) 
-  [Recommended 128-bit Custom UUID Management](https://assets.lairdtech.com/home/brandworld/files/Application%20Note%20-%20BLE%20Recommended%20128-bit%20Custom%20UUID%20Management.pdf) 
-  [Integrating Cayenne on TTN](https://assets.lairdtech.com/home/brandworld/files/Integrating%20Cayenne%20on%20TTN%20-%20RM1xx%20v1.1.pdf)
-  [The Things Network API Reference - CayenneLPP](https://www.thethingsnetwork.org/docs/devices/arduino/api/cayennelpp.html#class-cayennelpp)
-  [Send sensor data through The Things Network to Cayenne](https://hansboksem.wordpress.com/2017/03/06/sending-sensor-data-through-the-things-network-to-cayenne/)
-  [The Things Network - HTTP Integration](https://www.thethingsnetwork.org/docs/applications/http/)
-  [Base 64 converter](https://cryptii.com/pipes/base64-to-binary)

## Required wiring to flash and program the chip
| Laird RM186 | Serial to USB Converter |
|--|--|
| GND  | GND |
| UART TX (SIO_21) | RX
| UART RX (SIO_22) | TX
| UART RTS (SIO_23) | CTS
| UART CTS (SIO_24) | RTS
| VCC_BLE | VCC (3.3v)
| VCC_LORA | VCC (3.3v) 

**Note:** Make sure the VCC connected to the RM186 is 3.3v. I found that the VCC from the USB adapter was 3.596v and this was enough to disable the LoRa features. The LORAMACJoin function was failing with a non-zero return code of 29470. Once I performed a LORAMACGetOption on LORAMAG_OPT_SOURCE_VOLTAGE (#14) I could see the result “3596 mqV (FAIL)” was a problem. I used a voltage down converter to get closer to 3.3V and then, when I read LORAMAG_OPT_SOURCE_VOLTAGE it reported “3280 mV (PASS)”. At this point the LORAMACJoin worked and I was able to reach the gateway with packets.

**Note:** Regarding flow control. I found, when using my Serial to USB converter, that I had to disable flow control on the serial port in UwTerminalX to get the RM186 to respond. I assume because terminal was not getting a CTS signal from the RM186, the characters I was entering weren’t being transmitted. See page 21 of Datasheet (HIG) - RM1xx Series.pdf for the requirement for RTS/CTS connections. Providing a signal to the RM186 CTS and disabling flow control on the terminal application will work but I found that sometimes uploading a newly compiled program would fail, possibly because of a buffer overrun. In any case, it’s better to hook up the hardware flow control as described.
  
![alt text](images/uwterminalx.png "UwTerminalX")

![alt text](images/rm186pinout.png "UwTerminalX")

## Getting LoRa packets into the gateway using OTAA (Over The Air Activation)

You need to set the following registers in the MR186 LoRa chip (see page 8 of Interfacing with LoRaWAN - RM186 v1.3.pdf). 
Use the AT commands to do this from UwTerminalX:

Set the Application EUI:

Create a new application in TTN and add a new device. Then, see the “Device Overview” page in TTN for your new device:

![alt text](images/ttnparameters.png "UwTerminalX")

Note – these are examples from my gateway, use your own values on your own gateway

Set the Device EUI on the RM186 by entering the following into UwTerminalX (take the value from TTN device overview page "Device EUI"):

    at+cfgex 1011 "put 8 hex byte device eui here"

Next, set the Application EUI on the RM186 by entering the following into UwTerminalX (take the value from TTN device overview page "Application EUI"):

    at+cfgex 1010 "put 8 hex byte app eui here"

Now, set the Application Key by entering (take the value from TTN device overview page "App Key"):

    at+cfgex 1012 "put 16 hex byte app key here"

And finally, enter 

    atz

to make these settings permanent.

## smartBasic program requirements to send packets to the gateway

 - Set an event callback for EVLORAMACJOINING 
 - Set an event callback for EVLORAMACJOINED 
 - Set an event callback for EVLORAMACTXCOMPLETE 
 - Inside the event callback function for EVLORAMACJOINED you need to call LORAMACTxData
 - In the main program body: 
    - Call LORAMACReset 
    - Call LORAMACJoin(LORAMAC_JOIN_BY_REQUEST) // Use this
   parameter for OTAA

## Download the code and test yourself

Remember to set the three registeres, device eui, app eui, app key before you run the example.

Clone this project: [https://gitlab.com/ldaponte/lorasmartbasic.git](https://gitlab.com/ldaponte/lorasmartbasic.git)

Open UwTerminalX, make sure you have your RM186 board powered up and connected via your serial USB converter and that it is responding to AT commands.  Then, right click in the terminal window and select "Compile + Load".  A file dialog box will open and select the folder where you cloned the above repository and select the file loratest.sb.  You should see the following displayed in the terminal window:

 ```
 10	4	01 F1DF931A20C4
00
10	0	RM186
00
10	13	A780 BF25 
00

-- XCompile complete (1.56KB) --

-- Finished downloading file --
```
Then, type in the following to see that the file is on the RM186 board's file system:

```
at+dir

06	loratest
00
```

Now, run the loratest application by typing in the name of the program "loratest".  This sample will send the string "Hello World" to the application:

```
loratest
reset result was: 29449
Going to join LoRa
result was: 0
Attempting to join the LoRa network
Joined the LoRa network
lora tx result: 0
LoRa TX Complete Event
LoRa RX Complete Event
LoRa Received downstream data on port 1
RSSI: -66 SNR:33
hello world
```

You should now open the TTN dashboard and look at your gateway traffic.  You should see something like the following:
Note: the LoRa RX Complete event and the "Hello World" printed in the console here in my example is because I used the TTN console to send a downlink message.  If you don't do this then oyyu won't see the hello world or the RX event.

![alt text](images/ttntraffic.png "UwTerminalX")

That’s about it.

This repo contains a file called "getloraopts.sb" which will print the contents of all of the RM186 LoRa configuration options.  Here is what it printed for me:
Note: the "????" are the values from my device/app and I've removed the real values - you will see the values you entered in the above steps when you used the AT commands to set the device eui, app eui, and app key.

```
getloraopts
Set operation returned: 0

LORAMAC_OPT_TX_POWER: 5 dBm (1)
LORAMAC_OPT_DATA_RATE: 0 (2)
LORAMAC_OPT_JOIN_STATE: Not Joined (3)
LORAMAC_OPT_DEV_EUI: ???????????????? (4)
LORAMAC_OPT_CUSTOM_DEV_EUI:  (5)
LORAMAC_OPT_DEV_ADDR: 0 (6)
LORAMAC_OPT_APP_EUI: ???????????????? (7)
LORAMAC_OPT_APP_KEY: ???????????????????????????????? (8)
LORAMAC_OPT_VERSION:  (9)
LORAMAC_OPT_RSSI: 128 (10)
LORAMAC_OPT_SNR: 15 (11)
LORAMAC_OPT_DOWNLINK_COUNTER: 0 (12)
LORAMAC_OPT_UPLINK_COUNTER: 1 (13)
LORAMAG_OPT_SOURCE_VOLTAGE: 3280 mV (PASS) (14)
LORAMAC_OPT_915_HYBRID_MODE:  (15)
LORAMAC_OPT_BIRTHDAY: 25072017 (16)
LORAMAC_OPT_ADR_ENABLE: ADR is ENABLED (17)
LORAMAC_OPT_CHANNELLIST: Ch 0 868100 5 Ch 1 868300 5 Ch 2 868500 5 (18)
LORAMAC_OPT_CHANNELMASK: Channels Mask 0x0007 (19)
LORAMAC_OPT_NEXT_TX: Time to next transmission 0 (20)
LORAMAC_OPT_TEMPERATURE: Last Temp Reading = 0 deg C (21)
LORAMAC_OPT_TEMP_COMP_FACTOR: Temp Comp Factor = 11 deg C (22)
LORAMAC_OPT_FREQ_ERROR: Frequency Error = -3375 Hz (23)
LORAMAC_OPT_FREQ_OFFSET: Frequency Offset = 0 Hz (24)
LORAMAC_OPT_MAX_RETRIES: Maximum retries = 8 (25)
LORAMAC_OPT_DEVICE_CLASS:  (26)
LORAMAC_OPT_MAX_JOIN_ATTEMPTS:  (27)
LORAMAC_OPT_SUBBAND:  (28)
LORAMAC_OPT_NEXTTX_TIME:  (29)
LORAMAC_OPT_SEQUENCE_INCREMENT:  (30)
LORAMAC_OPT_EEPROM_UPCOUNTER:  (31)
LORAMAC_OPT_EEPROM_DOWNCOUNTER:  (32)
```

This will eventually be battery powered so the 3.3v step down, serial USB converter, and breadboard will be removed and wiring will be cleaned up.  Will also create a 3D printed case.

![alt text](images/cluge.png "UwTerminalX")